const gulp = require('gulp');
const sass = require('gulp-sass');
const browser = require('browser-sync').create();

function style()
{
    return gulp.src('./style/**/*.scss')
            .pipe(sass())
            .pipe(gulp.dest('./css')).pipe(browser.stream());

}
function watch()
{
    browser.init({
        server:{
            baseDir:'./'
        }
    })
    gulp.watch('./style/**/*.scss',style);
    gulp.watch('./*.html').on('change',browser.reload);
}

exports.watch=watch;
exports.style=style;